import objects
import os
from tqdm import tqdm
import json

"""
This script vectorizes the dataset and stores the result so that we don't have to do it again when implementing new models.
This is especially useful as this step takes a lot of time to run
"""

ROOT_DIRECTORY = '/Users/Moi/Desktop/Centrale/3A/Deep Learning/projet/data/cleaned/omnibail'
for type in ['train', 'test','validation']:
    dir =  os.path.join(ROOT_DIRECTORY, type)
    dataset = objects.FileListDataset(root_directory=dir, label_map={}) # label_map is useless in this situation , don't generate it for nothing
    all_examples = []
    for x in tqdm(dataset):
        all_examples.append({'sender_name':x['sender_name'], 'content':list(x['content'])})
    with open(os.path.join(ROOT_DIRECTORY, type + '.json'),'w') as f:
        json.dump(all_examples, f)