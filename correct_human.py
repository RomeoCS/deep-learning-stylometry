import numpy as np
import pandas as pd
import os

"""Process files filled by human participants to get their score on the authorship attribution tasks and store the results"""

def get_name(name, name_list):
    name = name.replace(' ','').lower() # enlever espaces
    for n in name_list:
        if name in n.lower():
            return n
    raise ValueError(name+' is not in the name list')

def clean_name(n, characters):
    for char, clean_char in characters.items():
        n = n.replace(char, clean_char)
    return n

ROOT = '/Users/Moi/Desktop/Centrale/3A/Deep Learning/projet/data/cleaned/omnibail'
answers_dir = os.path.join(ROOT, 'human', 'answers')
corrects = pd.read_csv(os.path.join(ROOT, 'human', 'examples_correct.csv'), sep='|')
special_characters = {'é':'e', 'ë':'e', 'ê':'e', 'è':'e'}
names = corrects.sender_name.apply(lambda n: clean_name(n, special_characters))
all_results = {}

for file_name in os.listdir(answers_dir):
    answers = pd.read_csv(os.path.join(answers_dir, file_name), sep='|', index_col=0)
    answer_names = answers['answer'].apply(lambda n: get_name(n, names))
    correct_answers = answer_names == names
    accuracy = (correct_answers).sum()
    
    results = pd.DataFrame(index=set(names.values), columns=['messages', 'predictions', 'TP','FP','FN']).fillna(0) 
    for lab, p in zip(names.values, answer_names.values):
        results.loc[lab, 'messages'] += 1
        results.loc[p, 'predictions'] += 1
        if lab == p :
            results.loc[lab, 'TP'] += 1
        else:
            results.loc[p, 'FP'] += 1
            results.loc[lab, 'FN'] += 1
    results['precision'] = 100 * results['TP'] / (results['TP'] + results['FP'])
    results['recall'] = 100 * results['TP'] / (results['TP'] + results['FN'])
    results['f1'] = np.sqrt(results['precision'] * results['recall'])
    results.loc['summary'] = results.multiply(results.messages, axis=0).sum() / results.messages.sum()
    
    answers['Bonne Réponse'] = answer_names
    answers['Correct'] = correct_answers.apply(lambda x: 'VRAI' if x else 'FAUX')
    answers.to_csv(os.path.join(ROOT, 'human', 'results', file_name))


    all_results[file_name[:-4]] = int(accuracy)

scores = pd.Series(all_results).sort_values(ascending=False)
ranks = scores.rank(ascending=False)
df = pd.DataFrame({'score':scores, 'rang':ranks})
df.to_csv(os.path.join(ROOT, 'human', 'results', 'final_results.csv'))

