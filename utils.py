import re
import numpy as np
from spacy.lang.fr.tag_map import TAG_MAP

# mapping from characters not recognized when downloading from FB
character_dictionary = {
    "\\x93":"oe", "\\x80\\x99":"'", "\\x87":"ç",
    "u\\x80":"ù", "e\\x80":"è", "a\\x80":"à",
    "u\\x81":"ú", "e\\x81":"é", "a\\x81":"á",
    "u\\x82":"û", "e\\x82":"ê", "a\\x82":"â",
    "\\x89":"É", "\\x82":"€",
}

# handles encoding issues from FB downloading
def clean_text(text):
    clean = text.encode('cp1252', errors='backslashreplace').decode('utf8', errors='ignore')
    for code, text in character_dictionary.items():
        clean = clean.replace(code, text)
    return clean

def get_features(text, tokens):
    FEATURES = []
    ### Character based features ###
    N_CHARACTERS = len(text)
    FEATURES.append(N_CHARACTERS) # 1 - character length
    FEATURES.append(sum(map(lambda x: x.isalpha(), text)) / N_CHARACTERS) # 2 - prop letters
    FEATURES.append(sum(map(lambda x: x.isupper(), text)) / N_CHARACTERS) # 3 - prop upper
    FEATURES.append(sum(map(lambda x: x.isdigit(), text)) / N_CHARACTERS) # 4 - prop digits
    FEATURES.append(sum(map(lambda x: x==' ', text)) / N_CHARACTERS) # 5 - prop blank spaces
    # ignore 6 (tabs) which is irrelevant
    # ignore 7 - 29 : this is just N_characters minus other features computed.abs
    # TODO : We could add a smiley count but it would be hard to encode

    ### Word-bases features ###
    N_WORDS = len(tokens)
    FEATURES.append(N_WORDS) # 30 - words length
    # ignore 31 - AVERAGE_LENGTH_PER_WORD : redundant with N_CHARACTERS and N_WORDS
    FEATURES.append(len(set([t.lemma_ for t in tokens])) / N_WORDS) # 32 - vocabulary richness
    FEATURES.append(sum(map(lambda x: len(x.text) >= 6, tokens)) / N_WORDS) # 33 - prop long words
    FEATURES.append(sum(map(lambda x: len(x.text) <= 3, tokens)) / N_WORDS) #  34 - prop short words
    # Ignore features 35-42 which are too complex (TODO : take into account)
    for length in range(20):
        FEATURES.append(sum(map(lambda x: len(x.text) == length, tokens)) / N_WORDS) # 43-62 distribution of word length
    # Ignore 63-140 - LIWC : only available in English and hard to encode

    ### Syntatic features ###
    SPECIAL_CHARACTERS = ["'",",",":",";",'"'] # special characters
    SPECIAL_CHARACTERS_MULT = ["\.","!","\?"] # special characters that can appear multiple times (!!! / ????? / ...)
    for char in SPECIAL_CHARACTERS: # 131 - 140
        FEATURES.append(len(re.findall(char, text)))
    for char in SPECIAL_CHARACTERS_MULT:
        occurrences = re.findall(char, text)
        single_occs = sum(map(lambda x:len(x)==1, occurrences))
        FEATURES.append(single_occs / N_CHARACTERS) # number of times the character is used alone
        FEATURES.append((len(occurrences)-single_occs) / N_CHARACTERS) # number of times the character is used multiple times

    ### Structural features ###
    # Structural features are mostly related to paragraphs structures. We only kept sentence related features and a ilst of words
    GREETING_WORDS = ['hello','salut','slt','wesh','bonjour','hi','hey','coucou']
    SLANG_WORDS = ['mdr','lol','xd','ptdr', 'haha', 'hihi', '^^']
    sentences = re.split('+|'.join(SPECIAL_CHARACTERS_MULT)+'+', text)
    sentences = [s for s in sentences if len(s) != 0]
    N_SENTENCES = len(sentences)
    FEATURES.append(N_SENTENCES) # 142 - number of sentences
    FEATURES.append(sum(map(lambda x:x[0].isupper(), sentences))) # 148 - prop of sentences beginning with uppercase
    text_lower = text.lower()
    for word in GREETING_WORDS + SLANG_WORDS: # 152 - should be 1 if a greeting word is presente. We chose to represent the presence of several words that reflect the style of someone : greeting words and slang words
        FEATURES.append(1 if word in text_lower else 0)
    
    ### Function words ###
    # We reduced the number of featuers by counting only the proportion of each pos instead of one bye pos word (for interjection and adposition mostly)
    all_pos = set([tag.split('__')[0] for tag in TAG_MAP.keys()])
    text_pos = [t.pos_ for t in tokens]
    for pos in all_pos:
        FEATURES.append(sum(map(lambda x: x==pos, text_pos))/N_WORDS)
    return np.array(FEATURES)

def one_hot_encode(array, voc, *args, **kwargs):
    """apply one hot encoding on the array, using *voc* size"""
    size = len(voc)
    shape = np.shape(array)
    encoded = np.zeros((shape[0],shape[1], size))
    for i, seq in enumerate(array):
        for j, value in enumerate(seq):
            encoded[i, j, value] = 1
    return encoded

def one_hot_encode_1D(array, size):
    shape = len(array)
    encoded = np.zeros((shape, size))
    for i, value in enumerate(array):
        encoded[i, value] = 1
    return encoded