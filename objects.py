import json
import numpy as np
import random
import os
from collections import defaultdict, Counter, OrderedDict
from time import time
from tqdm.notebook import tqdm

import torchtext
from torch.utils.data import Dataset, Sampler

############ DATASETS ############

class NLPDataset(Dataset):
    def __init__(self, label_map=None, *args, **kwargs):
        super(NLPDataset, self).__init__(*args, **kwargs)
        self.label_map = {}
        if label_map is None: # is no label map is passed, generate its own
            self.generate_label_map()
        else:
            self.label_map = label_map

    def generate_label_map(self):
        """generate a mapping between names of senders and numerical ids that will be processed by the algorithm"""
        labels_count = Counter()
        for message in self:
            labels_count.update([message['sender_name']]) # add label to the set of all possibilities
        sorted_labels = sorted(labels_count.items(), key=lambda x:-x[1])
        self.label_map = {label[0]:label_id for label_id, label in enumerate(sorted_labels)} # make a map from all possibilities


    def encode_name(self, name):
        """encode a sender name into its id"""
        if name in self.label_map.keys():
            return self.label_map[name]
        return -1 # if the name to encode is unknown

    def decode_id(self, code):
        """decode an id into its sender name"""
        for key, val in self.label_map.items():
            if val == code:
                return key
        return 'None' # if the id to decode is unknown

class FileListDataset(NLPDataset):
    """Loads data from a directory that contains one file per message in the form {"content":"blabla", "sender_name":"John Doe"}
    """
    def __init__(self, root_directory, label_map=None, *args, **kwargs):
        self.root_directory = root_directory
        super(FileListDataset, self).__init__(label_map=label_map, *args, **kwargs)

    def __len__(self):
        return len(os.listdir(self.root_directory))

    def __getitem__(self, i):
        file_name = os.listdir(self.root_directory)[i]
        file_path = os.path.join(self.root_directory, file_name)
        with open (file_path, 'r') as f:
            message = json.load(f)
        return {'content':message['content'], 'sender_name':message['sender_name'], 
            'label':self.encode_name(message['sender_name'])}

class MemoryFileListDataset(NLPDataset):
    """Loads data from a directory that contains one file per message in the form {"content":"blabla", "sender_name":"John Doe"}
    The content is tokenized and data is stored in self.data to avoid performance issues
    """
    def __init__(self, root_directory, tokenize, label_map=None, *args, **kwargs):
        self.root_directory = root_directory
        self.data = []
        for file_name in os.listdir(self.root_directory):
            file_path = os.path.join(self.root_directory, file_name)
            with open (file_path, 'r') as f:
                message = json.load(f)
            self.data.append({'content':tokenize(message['content']), 'sender_name':message['sender_name'],
                'label':self.encode_name(message['sender_name'])})

        super(FileListDataset, self).__init__(label_map=label_map, *args, **kwargs)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        return self.data[i]

class VectorDataset(NLPDataset):
    def __init__(self, root_directory, label_map=None, *args, **kwargs):
        self.root_directory = root_directory
        with open(root_directory, 'r') as f:
            self.data = json.load(f)
        super(VectorDataset, self).__init__(label_map=label_map, *args, **kwargs)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        message = self.data[i]
        return {'content':np.array(message['content']), 'sender_name':message['sender_name'], 
            'label':self.encode_name(message['sender_name'])}

############ SAMPLERS ############

class GroupSizeSampler(Sampler):
    """Sampler that orders by sentence size  the samples from an NLPDataset.
    This allows making batches of similar sizes and remove too short examples."""
    def __init__(self, dataset, field, min_size=0, shuffle=True):
        data_sizes = defaultdict(list)
        # for each data in dataset, compute its size and store it
        for idx, data in enumerate(dataset):
            content = data['content']
            size = len(field.tokenize(content))
            data_sizes[size].append(idx)
        self.sorted_samples = []
        for size, indexes in sorted(data_sizes.items(), key=lambda x:x[0]): # sort the data by size
            if size < min_size: # only add examples with at least min_size tokens
                continue
            if shuffle: # shuffle examples of same size
                random.shuffle(indexes)
            self.sorted_samples += indexes

    def __iter__(self):
        return iter(self.sorted_samples)

    def __len__(self):
        return len(self.sorted_samples)

class MemoryGroupSizeSampler(Sampler):
    """Sampler that orders by sentence size  the samples from an NLPDataset.
    This allows making batches of similar sizes and remove too short examples."""
    def __init__(self, dataset, min_size=0, shuffle=True):
        data_sizes = defaultdict(list)
        # for each data in dataset, compute its size and store it
        for idx, data in enumerate(dataset):
            content = data['content']
            size = len(content)
            data_sizes[size].append(idx)
        self.sorted_samples = []
        for size, indexes in sorted(data_sizes.items(), key=lambda x:x[0]): # sort the data by size
            if size < min_size: # only add examples with at least min_size tokens
                continue
            if shuffle: # shuffle examples of same size
                random.shuffle(indexes)
            self.sorted_samples += indexes

    def __iter__(self):
        return iter(self.sorted_samples)

    def __len__(self):
        return len(self.sorted_samples)

class CutSizeSampler(Sampler):
    """Sampler that keeps only examples of a minimum size and never contains more than *max_examples* examples.
    The input to that sampler must already be tokenized.
    """
    def __init__(self, dataset, min_size=0, max_examples=None, shuffle=True):
        if max_examples is None:
            max_examples = len(dataset)
        self.indexes = []
        n_examples = 0
        # for each data in dataset, compute its size and store it
        for idx, data in enumerate(dataset):
            size = len(data['content'])
            if size >= min_size: # only add examples with at least min_size tokens
                self.indexes.append(idx)
                n_examples += 1
            if n_examples >= max_examples: # stop when we have enough examples
                break
        
        if shuffle: # shuffle examples
            random.shuffle(self.indexes)

    def __iter__(self):
        return iter(self.indexes)

    def __len__(self):
        return len(self.indexes)

class VectorSizeSampler(Sampler):
    """Sampler that keeps only examples of a minimum size and never contains more than *max_examples* examples.
    This sampler takes vectorized texts as input"""
    def __init__(self, dataset, min_size=0, max_examples=None, shuffle=True):
        if max_examples is None:
            max_examples = len(dataset)
        self.indexes = []
        n_examples = 0
        # for each data in dataset, compute its size and store it
        for idx, data in enumerate(dataset):
            size = data['content'][5]
            if size >= min_size: # only add examples with at least min_size tokens
                self.indexes.append(idx)
                n_examples += 1
            if n_examples >= max_examples: # stop when we have enough examples
                break
        
        if shuffle: # shuffle examples
            random.shuffle(self.indexes)

    def __iter__(self):
        return iter(self.indexes)

    def __len__(self):
        return len(self.indexes)

############ FIELDS ############

class CustomField(torchtext.data.Field):
    def __init__(self, *args, **kwargs):
        super(CustomField, self).__init__(*args, **kwargs)

    def build_vocab(self, dataset, use_tqdm=False, *args, **kwargs):
        """build the vocabulary from all the examples in the dataset"""
        counter = defaultdict(int)
        if use_tqdm:
            for data in tqdm(dataset):
                words = [token for token in self.tokenize(data['content'])]
                for word in words:
                    counter[word.lower()] += 1
        else:
            for data in dataset:
                words = [token for token in self.tokenize(data['content'])]
                for word in words:
                    counter[word.lower()] += 1
                    
        specials = list(OrderedDict.fromkeys(
            tok for tok in [self.unk_token, self.pad_token, self.init_token,
                            self.eos_token] + kwargs.pop('specials', [])
            if tok is not None))
        for spec in specials:
            counter[spec] = 0
        self.vocab = self.vocab_cls(counter, specials=specials, **kwargs)

    def process_batch(self, batch, device=None):
        """apply preprocessing and processing for a batch"""
        batch = [self.preprocess(x) for x in batch]
        return self.process(batch, device=device)

